bl_info = {
    "name": "Smart Terrain Editor",
    "author": "Giuliani Daniele",
    "version": (0, 1),
    "blender": (2, 90, 1),
    "location": "Properties > World > Smart Terrain Editor",
    "description": "Adds a new Fantasy Terrain",
    "warning": "WIP",
    "wiki_url": "https://gitlab.com/d.giuliani304/smartterraineditor/-/wikis/home",
    "tracker_url": "https://gitlab.com/d.giuliani304/smartterraineditor/-/issues",
    "category": "3D View",
}
import bpy
from mathutils import Vector
from bpy.props import FloatVectorProperty
from bpy.props import EnumProperty
from bpy.props import BoolProperty
from bpy.props import IntProperty
from bpy_extras import object_utils

#***********************************#
# DRAW Preferences in the addon tab #
#***********************************#
class ST_EDITOR_preferences (bpy.types.AddonPreferences):
    bl_idname = 'st_editor.preferences'
    terrain_quality_pref : EnumProperty(
        name = "terrain_quality_pref",
        items = [('Basic','Basic','','',0),
                ('Normal','Normal','','',1),
                ('Advanced','Advanced','','',2),
                ('Epic','Epic','','',4)
        ],
        default = 'Basic'
    )
    in_edit : BoolProperty(
        name = "ST_edit_can",
        default = False
    )
    def draw(self, context):
        layout = self.layout
        layout.label(text='Set default quality of the Terrain:')
        row = layout.row()
        row.prop(self, 'terrain_quality_pref', expand=True)

#***********************************#
# Operators #
#***********************************#
class ST_EDITOR_OT_generate_terrain(bpy.types.Operator, object_utils.AddObjectHelper):
    """Generate Terrain"""
    bl_idname = "st_editor.generate_terrain"
    bl_label = "ST Editor"
    bl_description = "Create Terrain Object Types"
    bl_options = {'REGISTER', 'UNDO'}
    type: EnumProperty(
        name="Type",
        description="Set Type",
        items=[("Tris", "Tris", "Triangulate Terrain"),
               ("Quad", "Quad", "Quadrangular Terrain"),
               ("Hex", "Hex", "Hexagonal grid Terrain"),
               ("Vox", "Vox", "Voxelize Terrain"),
               ("Help", "Help", "Not implemented"),
              ],
        default='Quad'
        )
    res: IntProperty(
        name="Resolution x/y",
        min=3, max=32,
        description="Number of faces in a chunk x/y",
        default=8)
    size: IntProperty(
        name="Size x/y",
        min=3, max=32,
        description="Number of chunk x/y",
        default=8)
    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = layout.column()
        col.prop(self, 'type')
        col = layout.column()
        col.prop(self, 'align')
        split = layout.split()
        row = layout.row()
        row.label(text="Resolution:")
        row.label(text="Size:")
        row = layout.row()
        row.prop(self, "res")
        row.prop(self, "size")
        layout.row().label(text='Location:')
        row = layout.row()
        row.prop(self, 'location', text='')

    def execute(self, context):
        props = self.properties
        # TODO: make
        gap = 0.5*props.size
        for x in range(props.size):
            for y in range(props.size):
                bpy.ops.mesh.primitive_grid_add(
                    x_subdivisions=props.res,
                    y_subdivisions=props.res,
                    size=props.size,
                    enter_editmode=False,
                    align=props.align,
                    location=(
                        props.location.x + x*props.size - (props.size/2) + gap,
                        props.location.y + y*props.size - (props.size/2) + gap,
                        props.location.z
                    ),
                    scale=(1, 1, 1)
                )

        #context.active_object.location = prop.location
        return {'FINISHED'}
    def invoke(self, context, event):
        render = context.scene.render
        render.resolution_x = 1024
        render.resolution_y = 768
        render.resolution_percentage = 100

        # Call image editor window
        bpy.ops.render.view_show("INVOKE_DEFAULT")

        # Change area type
        area = context.window_manager.windows[-1].screen.areas[0]
        area.type = "VIEW_3D"
        if context.space_data.type == 'VIEW_3D':
            v3d = context.space_data
            v3d.show_gizmo = False
            v3d.shading.type = 'WIREFRAME'
            v3d.overlay.show_text = False
            v3d.overlay.show_extras = False
            rv3d = v3d.region_3d
            props = self.properties
            # TODO: make
            self.execute(context)
            return {'FINISHED'}
        else:
            self.report({'WARNING'}, "Active space must be a View3d")
            return {'CANCELLED'}

#***********************************#
# PANEL #
#***********************************#
class ST_EDITOR_PT_panel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Smart Terrain Editor"
    bl_idname = "ST_EDITOR_PT_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "world"
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.operator("st_editor.generate_terrain")

#***********************************#
# REGISTER STUFF #
#***********************************#
_classes = [
    ST_EDITOR_preferences,
    ST_EDITOR_OT_generate_terrain,
    ST_EDITOR_PT_panel,
]

def register():
    for cls in _classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(_classes):
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()
