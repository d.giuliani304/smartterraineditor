# Example of creating a polygonal mesh in Python from numpy arrays
# Note: this is Python 3.x code
#
# $ blender -P create_mesh.py
#
# See this link for more information on this part of the API:
# https://docs.blender.org/api/blender2.8/bpy.types.Mesh.html
#
# Paul Melis (paul.melis@surfsara.nl), SURFsara, 24-05-2019
import bpy
import numpy

# Note: we DELETE all objects in the scene and only then create the new mesh!
# bpy.ops.object.select_all(action='SELECT')
# bpy.ops.object.delete()

# Utility functions
def vert(column, row, size):
    """ Create a single vert """
    return (column * size, row * size, 0)
def vert_col(column, row):
    """ Create a single vert """
    return (1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0)
def gen_uv(column, row):
    """ Create a single UV vert """
    return (0, 0, 1, 0, 1, 1, 0, 1)
def face(column, row, rows):
    """ Create a single face """
    return (column* rows + row, (column + 1) * rows + row, (column + 1) * rows + 1 + row, column * rows + 1 + row)

# Vertices and edges (straightforward)
class GenMesh():
    """Class GenMesh. TODO"""

    def __init__(self):
        # Settings
        name = 'Gridtastic'
        rows = 5
        columns = 10
        size = 1

        # Looping to create the grid
        vertices = numpy.array([vert(x, y, size) for x in range(columns) for y in range(rows)], dtype=numpy.float32)
        vertex_index = numpy.array([face(x, y, rows) for x in range(columns - 1) for y in range(rows - 1)], dtype=numpy.int32)
        uv_coordinates = numpy.array([gen_uv(x, y) for x in range(columns) for y in range(rows)], dtype=numpy.float32)
        vertex_colors = numpy.array([vert_col(x, y) for x in range(columns) for y in range(rows)], dtype=numpy.float32)
        # vertices = numpy.array([
        #     0, 0, 0,
        #     2, 0, 0,
        #     2, 2, 0.2,
        #     0, 2, 0.2,
        # ], dtype=numpy.float32)

        # Setting edges is optional, as they get created automatically for
        # any provided polygons. However, if you need edges that exist separately
        # from polygons then use this array.
        # XXX these edges only seem to show up after going in-and-out of edit mode.

        num_vertices = vertices.shape[0] // 3

        # Polygons are defined in loops. Here, we define one quad and two triangles

        # vertex_index = numpy.array([
        #     0, 1, 2, 3,
        # ], dtype=numpy.int32)

        # For each polygon the start of its vertex indices in the vertex_index array
        loop_start = numpy.array([
            0
        ], dtype=numpy.int32)

        # Length of each polygon in number of vertices
        loop_total = numpy.array([
            4
        ], dtype=numpy.int32)

        num_vertex_indices = vertex_index.shape[0]
        num_loops = loop_start.shape[0]

        # Texture coordinates per vertex *per polygon loop*.
        # uv_coordinates = numpy.array([
        #     0, 0,
        #     1, 0,
        #     1, 1,
        #     0, 1,
        # ], dtype=numpy.float32)

        # Vertex color per vertex *per polygon loop*
        # vertex_colors = numpy.array([
        #     1, 0, 0,
        #     1, 0, 0,
        #     1, 0, 0,
        #     1, 0, 0,
        # ], dtype=numpy.float32)
        print(uv_coordinates.shape[0], 2*vertex_index.shape[0])
        print(vertex_colors.shape[0], 3*vertex_index.shape[0])
        assert uv_coordinates.shape[0] == 2*vertex_index.shape[0]
        assert vertex_colors.shape[0] == 3*vertex_index.shape[0]

        # Create mesh object based on the arrays above

        mesh = bpy.data.meshes.new(name='created mesh')

        mesh.vertices.add(num_vertices)
        mesh.vertices.foreach_set("co", vertices)


        mesh.loops.add(num_vertex_indices)
        mesh.loops.foreach_set("vertex_index", vertex_index)

        mesh.polygons.add(num_loops)
        mesh.polygons.foreach_set("loop_start", loop_start)
        mesh.polygons.foreach_set("loop_total", loop_total)

        # Create UV coordinate layer and set values
        uv_layer = mesh.uv_layers.new()
        for i, uv in enumerate(uv_layer.data):
            uv.uv = uv_coordinates[2*i:2*i+2]

        # Create vertex color layer and set values
        vcol_lay = mesh.vertex_colors.new()
        for i, col in enumerate(vcol_lay.data):
            col.color[0] = vertex_colors[3*i+0]
            col.color[1] = vertex_colors[3*i+1]
            col.color[2] = vertex_colors[3*i+2]
            col.color[3] = 1.0                     # Alpha?

        # We're done setting up the mesh values, update mesh object and
        # let Blender do some checks on it
        mesh.update()
        mesh.validate()

        # Create Object whose Object Data is our new mesh
        obj = bpy.data.objects.new('created object', mesh)

        # Add *Object* to the scene, not the mesh
        scene = bpy.context.scene
        scene.collection.objects.link(obj)

        # Select the new object and make it active
        bpy.ops.object.select_all(action='DESELECT')
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
